<?php

// ----------------------------- 
//	Ce script est en charge de l'analyne des fichiers Excel, de la création des fichiers JSON, et du .zip qui les empaquête.
//  Il est appellé par des appels Ajax depuis l'outil de convertision Excel->JSON
//	Les fichiers Excels en entrées doivent répondre strictement à la norme documenté. Ils decrivent le parcours décisionnel pour definir la Classification Conventionnelle.
//  Le fichier JSON en sortie sont destinés à alimenter l'application Classification Conventionnelle.
//  ----------------------------

define('ROOTPATH', dirname(__FILE__) );
define('IMPORT_PATH', ROOTPATH.'/imports/');
define('EXPORT_PATH', ROOTPATH.'/exports/');
define('INDEX_FILENAME', 'CONVENTIONS.xlsx');
define('ZIPFILE_PATH', ROOTPATH.'/pack-json.zip');

date_default_timezone_set('Europe/Paris'); 
ini_set('default_charset', 'UTF-8');
ini_set('display_errors', 1);
error_reporting(E_ALL);
ini_set("log_errors" , "1");
ini_set("max_execution_time", 100);
ini_set("error_log" , ROOTPATH . "/server-errors.log" );
$loader = require( ROOTPATH.'/vendor/autoload.php');


// La classe Report n'a pour but que de récupérer les Erreur et Warning lors des diverses fonctions (l'analyse des fichiers .xlsx, l'import, etc.)
// Elle constitue une pile d'erreur/warning qu'on pourra facilement renvoyer vers le front-end pour l'affichage
class Report { 

	private static $stack = [];

	private static function push($mess, $type=0, $details=false) {
		if ($type<1) $_type = "ERROR";
		if ($type==1) $_type = "WARNING";
		if ($type==2) $_type = "NOTICE";
		array_push( self::$stack, array("date"=>date( "d/m/Y H:i:s", time() ), "type"=>$_type, "message"=>$mess, "details"=>$details ) );
		if ($type<0) self::end();
	}

	public static function notice($mess, $details=false) { self::push($mess, 2, $details); return false; }
	public static function   warn($mess, $details=false) { self::push($mess, 1, $details); return false; }
	public static function   kill($mess, $details=false) { self::push($mess, -1, $details); return false; }
	public static function  error($mess, $details=false) { self::push($mess, 0, $details); return false; }
	
	public static function getStack() {
		return self::$stack;
	}

	public static function hasError() {
		$has = false;
		foreach(self::$stack as $k=>$v) {
			if ($v['type']==='ERROR') {
				$has=true;
				break;
			}
		}
		return $has;
	}

	public static function end($data=false) {
		$has_error = false;
		foreach(self::$stack as $k=>$v) 
			if ($v['type']==="ERROR") { $has_error=true; break; }
		$out = array("success"=> !$has_error, "stack"=>self::getStack() );
		if ($data && is_array($data)) $out = array_merge($out, $data);
		echo json_encode($out);
		die();
	}
}



// La classe ExcelData (qui s'appuie sur la bibliothéque PHPExcel) :
// - elle lit et simplifie le parcours des données dans la table (getTable, getLabel...)
// - elle analyse les données du fichier Excel, les formatte, et les exporte en Json
class ExcelData {

	public $sheets = [];
	public $filepath;

	public function __construct($filepath) {

		if (!$filepath) return Report::error("Aucun fichier fourni.");
		$this->filepath = $filepath;
		
		if (!file_exists($this->filepath) )
			return Report::error("Le fichier ".basename($this->filepath)." n'existe pas.");
		try {

			$objPHPExcel = PHPExcel_IOFactory::load($this->filepath);
			foreach ($objPHPExcel->getAllSheets() as $sheet) { // Pour chaque feuille créer une entrée dans l'array  $sheets 
				$this->sheets[ $sheet->getTitle() ] = $this->getTable($sheet);
			}			
			
		} catch(Exception $e) {
			Report::error("Impossible de lire le fichier '".basename($this->filepath)."'. Êtes-vous sur que ce fichier est un fichier Excel valide ?");
		}
		
		if (Report::hasError()) Report::end();
	}

	// Transforme une feuille ($sheet) de PHPExcel en un Array.
	private function getTable($sheet) {
		$it = $sheet->getRowIterator();
		$table = array();
		$lastRowIsEmpty = false;
		foreach( $it as $r=>$row) { // Pour chaque ligne de la feuille...
			$itC = $row->getCellIterator();
			$rowContent = array();
			$lastCellIsNull = false;
			foreach( $itC as $c=>$cell){ // Pour chaque cellule de la ligne

				if (is_null($cell->getValue())) { // Est-ce une cellule vide ?
					if ($lastCellIsNull) { break; } // Deux cellules vides consécutives déclenchent un passage à la ligne suivante
						else $lastCellIsNull=true; 
				} else $lastCellIsNull=false;

				$rowContent[] = $cell->getValue(); 
			}
			if ($lastCellIsNull) array_pop($rowContent); // Supprimer la dernière entrée car elle est vide

			if (empty($rowContent)) { // Est-ce que la ligne est vide ?
				if ($lastRowIsEmpty) break; // Deux lignes vides consécutives et tu passes à la feuille suivante
				else $lastRowIsEmpty=true;
			} else $lastRowIsEmpty=false;

			$table[] = $rowContent;
		}
		if ($lastRowIsEmpty) array_pop($table); // Supprimer la dernière entrée car elle est vide
		return $table;
	}

	public function getLabel($row) { // Obtenir le label, qui est dans la première cellule d'une ligne
		return strtolower(trim($row[0]));
	}

	private static function replaceLineBreak($str) {
		$str = str_replace( array("\n\n", "\n"), "<br/>", trim($str) );
		return $str;
	}


	private function createConventionJson($conventionIndex, $sheets) {

		$conv = array("questions"=>array(), "results"=>array());
		foreach($sheets as $index=>$sheet) { // parcours des feuilles du fichier Excel

			if ($index==="RESULTS") { // Si c'est la feuille RESULTS :
				$type = "results";
				foreach($sheet as $k_row=>$row) { // parcours des lignes de la feuille RESULTS : 
					if ($this->getLabel($row) === "type" && strtolower(trim($row[1]))!=='results' )  
						Report::warning("La valeur 'type' donné dans la feuille RESULTS de la convention $index n'est pas 'results' mais '".$row[1]."'.");
					if ($this->getLabel($row) === "score" || $this->getLabel($row) === "type" || empty($row) ) continue;
					$conv["results"][ $row[0] ] = array_slice($row, 1);
					
				}
			} else { // Si c'est une autre feuille que RESULTS :
				$type = "block-title-description";
				$question = false;
				$label = false;
				$choices = array();
				foreach($sheet as $k_row=>$row) { // parcours des lignes de la feuille
					if ($this->getLabel($row) === "type" && isset($row[1]) ) $type = trim($row[1]);
					else if ($this->getLabel($row) === "question" && isset($row[1])) $question = self::replaceLineBreak($row[1]);
					else if ( strpos($this->getLabel($row), "label")===0 && isset($row[1]) ) $label = self::replaceLineBreak($row[1]);
					else if ($this->getLabel($row) === "index") $choices = $this->getChoicesData($type, $sheet, $k_row, $index, $conventionIndex);
					if ($choices) break;
				}
				$conv['questions'][] = array(
					"id"=>trim($index),
					"type"=>trim($type),
					"_title"=>$question,
					"_label"=>$label,
					"choices"=>$choices
				);
			}
		}

		// Écrire au format JSON dans le fichier (le fichier est créé s'il n'existe pas)
		$json_filename = EXPORT_PATH.$conventionIndex.".json";
		$file_handler = fopen($json_filename, "w");
		fwrite($file_handler, json_encode($conv, JSON_PRETTY_PRINT) );
		fclose($file_handler);
		return $json_filename;
	}


	private function getChoicesData($type, $sheet, $offset, $sheetName, $convName) {

		$choices = array();
		
		if ($type==="block-title-description") {

			foreach($sheet as $k_row=>$row) { // parcours des lignes de la feuille
				if ($offset >= $k_row) continue;
				if ( count($row)<5 ) {
					Report::warn("La ligne ".$k_row." de la feuille '".$sheetName."' (convention '".$convName."') ne semble pas complète...");
					continue;
				}

				$newChoice = array(
					"_title"=>trim($row[1]),
					"_description"=> self::replaceLineBreak($row[2]),
					"_point"=>floatval($row[3]),
					"_jumpTo"=>trim($row[4])
				); 

				if ($row[1] != $row[0]) $newChoice['_index'] = trim($row[0]); 

				$choices[] = $newChoice;
			}

		} else if ($type==="sliding-block") {
			$indexes = array();
			foreach($sheet as $k_row=>$row) { // parcours des lignes de la feuille
				
				if ($offset === $k_row) {
					$indexes = array_slice($row,3);
					continue;
				} else 
					if ($offset > $k_row) continue;
				
				if (!$indexes || !count($indexes)) {
					Report::warn("Une feuille '".$sheetName."' (convention '".$convName."') de type 'sliding-block-with-categories' ne spécifie aucune categorie.");
					break; 
				}

				$newChoice = array(
					"_index"=>trim($row[0]),
					"_point"=>floatval($row[1]),
					"_jumpTo"=>trim($row[2])
				); 

				foreach($indexes as $ki=>$ind) 
					$newChoice[$ind] = self::replaceLineBreak($row[3+$ki]);
				
				$choices[] = $newChoice;

			}
		} 

		// Quelques verifications...
		foreach($choices as $kc=>$c) {

			if (isset($c['_jumpTo']) && !isset($this->sheets[$c['_jumpTo']]) ) {
				Report::warn("Il semblerait que la feuille <code>".$c['_jumpTo']."</code> n'existe pas alors qu'elle est citée dans la feuille ".$sheetName.". Vérifiez que le nom de la feuille ne soit pas simplement mal orthographiée ou tronqué (ils sont limités à 30 caractères sur MS-Excel).");
			}

			if (isset($c['_point']) && is_nan($c['_point']) ) {
				Report::warn("Dans la feuille ".$sheetName.", veuillez verifier les ponderations, elles ne semblent pas toutes valide.");
			}
		}

		return $choices;
	}



	public function updateFile() {
		
		$filename = basename($this->filepath);
		if (strtolower($filename)!==trim(strtolower(INDEX_FILENAME))) { // Si ce n'est pas le fichier conventions.xlsx
		
			if (!isset($this->sheets['RESULTS'])) 
				Report::error("Il n'y a pas de feuilles nommées RESULTS dans le fichier Excel fourni.");
		
			$conventionIndex = pathinfo($this->filepath)['filename'];
			$exists_yet = file_exists(EXPORT_PATH.$conventionIndex.".json");
			$this->createConventionJson( $conventionIndex , $this->sheets);

			if ($exists_yet) 
				Report::notice("Un fichier ayant le nom '".$filename."' existe déjà. Il vient donc d'être remplacé.");	
			else 
				Report::notice("Si ce n'est pas déjà fait, n'oubliez pas d'indexer cette nouvelle convention dans le fichier <code>CONVENTIONS.xlsx</code> (l'index des conventions) afin qu'elle puisse apparaître dans l'application.");
			
		} else {

			$conventions = current( $this->sheets );
			if (!$conventions || !count($conventions)) Report::kill("Impossible de trouver une seule convention dans l'index des conventions.");
			array_shift($conventions); // Retirer l'entête
			$conventions = array_map(function($a){
				
				if ( !isset($a[0]) || empty($a[0]) ) return false;
				
				if (!isset($a[2])) $a[2] = "book"; // valeur par defaut
				if (!isset($a[3])) $a[3] = "non"; // valeur par defaut

				$is_enabled = (strtolower($a[3])==='oui' ? true : false);

				$r = array(
					'_index'=>$a[0], 
					'_title'=>$a[1], 
					'_icon'=>$a[2], 
					'_disabled'=>!$is_enabled
				); 

				$file = IMPORT_PATH.$a[0].".xlsx";				
				if ( !file_exists($file) ) {
					$nonexists_text = "Le fichier <code>".$a[0].".xlsx</code> n'existe pas alors qu'il est référencé par l'index <code>".$a[0]."</code> dans CONVENTIONS.xlsx";
					if ($is_enabled) {
						Report::error($nonexists_text);
						return false;
					} else {
						Report::warn($nonexists_text);
					}
				} else {
					$r['_last_change'] = filemtime($file);
				}
				return $r;
					
			}, $conventions);

			// Écrire au format JSON dans le fichier 'conventions.json' (le fichier est créé s'il n'existe pas)
			$json_filepath = EXPORT_PATH."conventions.json";

			$file_handler = fopen($json_filepath, "w");
			fwrite($file_handler, json_encode($conventions, JSON_PRETTY_PRINT) );
			fclose($file_handler);
		}

	}

}





// ACTIONS :
// ---------


// RENVOIT LA LISTE des fichiers .xlsx contenu dans le dossier IMPORT_PATH
function listFiles() {

	// Verification des dossiers IMPORT / EXPORT
	if (!file_exists(IMPORT_PATH)) Report::kill("Le dossier qui contient les fichiers .xlsx n'existe pas.");
	if (!file_exists(EXPORT_PATH)) {
		mkdir(EXPORT_PATH);
		if (!file_exists(IMPORT_PATH)) Report::kill("Le dossier qui contient les fichiers convertis (json) ne peut pas être créé.");
	}

	// Parcours du dossier IMPORT et renvoit la liste des fichiers .xlsx qu'il contient
	$list = array();
	if ($handle = opendir(IMPORT_PATH)) {
	    while (false !== ($entry = readdir($handle))) { // Pour tous les fichiers contenu dans le dossier IMPORT_PATH
	    	if (substr($entry, 0, 1)==='~') { @unlink(IMPORT_PATH.$entry); continue; } // Supprime les fichiers tmp de Excel s'il y en a
	        $ext = pathinfo(IMPORT_PATH.$entry)['extension'];
			if ($ext!=="xlsx") continue;
	        $list[] = $entry;
	    }
	    closedir($handle);
	}
	Report::end(array('list'=>$list));
}


// TELECHARGE le fichier Excel nommé dans le paramètre GET 'file'
function downloadOne() {

	$one = isset($_REQUEST['file']) ? $_REQUEST['file'] : false;
	if (!$one || !file_exists(IMPORT_PATH.$one)) 
		Report::kill("Le fichier demandé n'existe plus.");

	// Force le télechargement du fichier par le navigateur
	header('Content-Type: application/octet-stream');
	header("Content-Transfer-Encoding: Binary"); 
	header("Content-disposition: attachment; filename=\"" . basename(IMPORT_PATH.$one) . "\""); 
	readfile(IMPORT_PATH.$one);
}

// SUPPRIME le fichier Excel nommé dans le paramètre GET 'file'
function deleteOne() {

	$one = isset($_REQUEST['file']) ? $_REQUEST['file'] : false;
	if (!$one || !file_exists(IMPORT_PATH.$one)) 
		Report::kill("Le fichier demandé n'existe déjà plus.");

	$one = str_replace( array("/", ".."), array(""), $one); // Retire les / et les .. du nom du fichier pour éviter l'accès à d'autres dossiers 
	unlink(IMPORT_PATH.$one); // Supprime le fichier .xlsx

	// Suppression du fichier Json associé, s'il existe : 
	if (strtolower($one)!==trim(strtolower(INDEX_FILENAME))) { // Si ce n'est pas le fichier conventions.xlsx
		$jsonFile = EXPORT_PATH.str_replace(".xlsx", ".json", $one);
	} else { // Si c'est le fichier INDEX_FILENAME
		$jsonFile = EXPORT_PATH."conventions.json"; 
	}
	if (file_exists($jsonFile)) unlink($jsonFile); 

	Report::end();
}


// GERE L'IMPORT d'un fichier uploadé
// Declenche l'analyse du fichier afin de créer le JSON associé dans le fichier EXPORT_PATH
// Si aucune erreur bloquante n'est détectée, on peut créer le fichier .zip avec les Json générés
function import() {

	if (!$_FILES || empty($_FILES) || !isset($_FILES['file']) || !is_array($_FILES['file']) )
		Report::kill("Aucun fichier à importer");

	$has_imported = false;

	$file = $_FILES['file'];
	$ext = pathinfo($file['name'])['extension'];
	if ($ext!=="xlsx") {
		unlink($file['tmp_name']);
		Report::kill("Seuls les fichiers au format .xlsx peuvent être importés ici.");
	}
		
	$filepath = IMPORT_PATH.str_replace( array(" ", "/"), array("-", ""), $file['name'] );
	
	move_uploaded_file( $file['tmp_name'] , $filepath ); 
	if (!file_exists($filepath)) {
		Report::kill("Impossible d'importer le fichier ".$file['name']);
		@unlink($file['tmp_name']);
	} else {
		$excelFileParser = new ExcelData( $filepath );
		$excelFileParser->updateFile();
	}

	if (Report::hasError()) {
		if (file_exists($file['tmp_name'])) unlink($file['tmp_name']);
		unlink($filepath);
	} else {
		if (file_exists(ZIPFILE_PATH)) unlink(ZIPFILE_PATH);
		$zip = new ZipArchive();
		if ($zip->open(ZIPFILE_PATH, ZipArchive::CREATE)!==TRUE) 
		    Report::kill("Impossible de créer le fichier .zip");
		$list = array();
		if ($handle = opendir(EXPORT_PATH)) {
		    while (false !== ($entry = readdir($handle))) {
		        $ext = pathinfo(EXPORT_PATH.$entry)['extension'];
				if ($ext!=="json") continue;
		        $zip->addFile( EXPORT_PATH.$entry, "/".$entry);
		    }
		    closedir($handle);
		} else Report::kill("Impossible de trouver les fichiers .JSON");
		$zip->close();
	}

	
	Report::end();
}


// FORCE LE TELECHARGEMENT des fichiers Json générés lors des imports
function export() {
	header('Content-Type: application/octet-stream');
	header("Content-Transfer-Encoding: Binary"); 
	header("Content-disposition: attachment; filename=\"" . basename(ZIPFILE_PATH) . "\""); 
	readfile(ZIPFILE_PATH);
	Report::end();
}	



// ROUTING / SECURITÉ : selon le paramètre GET "action" passé, on appelle l'une des 5 actions possibles : 
$action = isset($_REQUEST['action']) ?  $_REQUEST['action'] : false;
if (!$action || !in_array($action, ['listFiles', 'deleteOne', 'downloadOne', 'import', 'export']) ) Report::kill("Commande inconnue.");
$action();




