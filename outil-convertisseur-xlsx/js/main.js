

var app;



Dropzone.autoDiscover = false;


$( document ).ready(function() {
	
	var modal = $('.modal');
	var is_loading = false;

	var showModal = function(callback) {
		$('.onsuccess, .onsuccess-import, .onfail, .onfail-common, .modal-header .custom-title, .onfail-import, .stack', modal).hide();
		callback();
		$('.modal').modal('show');
	}

	var onError = function(err, title) {
		var message = (err && err.message ? err.message : err);
		$('.onfail-common .error-content', modal).html( err );
			
		showModal(function() {
			$('.onfail, .onfail-common', modal).show();
			if (title) { 
				$('.modal-header .onfail', modal).hide();
				$('.modal-header .custom-title', modal).show().html(title);
			}
		});
	}


	var onSuccess = function(data, file) {
		
		var stack = "";
		if (data.stack && data.stack.length) {
			$.each(data.stack, function(k,line) {
				var type = line.type==="ERROR" ? "danger" : (line.type==="WARNING" ? "warning" : "info");
				var typeLabel = line.type==="ERROR" ? "Erreur" : (line.type==="WARNING" ? "À corriger" : "Information");
				stack += '<div class="l"><span class="label label-'+type+'">'+typeLabel+'</span>'+line.message+'</div>';
			});
		}
		$('.modal-body .stack', modal).html(stack);

		showModal(function() {
			$('.stack', modal).show();
			if (data.success) {
				$('.onsuccess, .onsuccess-import', modal).show();
				$('.onfail, .onfail-import', modal).hide();
			} else {
				$('.onsuccess, .onsuccess-import', modal).hide();
				$('.onfail, .onfail-import', modal).show();
			}
		});
	}


	var myDropzone = new Dropzone("form#dropzone", {
		paramName: "file", // The name that will be used to transfer the file
		uploadMultiple: false,
		maxFiles: 1,
		maxFilesize: 2, // MB
		autoProcessQueue: true,
		clickable: '#dropzone, #dropzone #upload-incentive *',
		previewTemplate: $('script#templateUploadingFile').html(),
		dictDefaultMessage: "Déposez ici les fichiers .xlsx à intégrer",
		dictFallbackMessage: "", // "Votre navigateur ne permet pas de glisser-déposer les fichiers dans la zone.",
		dictFallbackText: "Veuillez cliquer sur le bouton pour choisir le fichier à importer.",
		dictFileTooBig: "Le fichier à importer est trop lourd ({{filesize}}Mo). La taille maximale autorisée est de {{maxFilesize}}Mo.",
		dictInvalidFileType: "Le type de fichier soumis n'est pas pris en compte. Désolé.",
		dictResponseError: "Une erreur ({{statusCode}}) est survenue.",
		dictCancelUpload: "Annuler",
		dictCancelUploadConfirmation: "Êtes-vous sur de vouloir annuler ?",
		dictRemoveFile: "Supprimer le fichier",
		dictRemoveFileConfirmation: null,
		dictMaxFilesExceeded: "La limite d'importation a été atteinte.",
		init: function() {

			this.on("addedfile", function(file) { 
				$("#dropzone #upload-incentive").hide(); 
			});

			this.on("success", function(file, r) { 
				
				r = jQuery.parseJSON(r);

				if (typeof r.success !== 'boolean')
					onError(r);
				else {
					updateList();
					onSuccess(r, file);
				}
					

				file.previewElement.remove();

				if ( !$('#dropzone .icon-file').length ) this.options.customReset();

			});

			this.on("error", function(file, r) { 
				file.previewElement.remove();
				if ( !$('#dropzone .icon-file').length ) this.options.customReset();
			});

			this.on("maxfilesexceeded", function() { 
				$("#dropzone .icon-file").each(function(k,v) {
					if (k>0) $(v).remove();
				})
			});
			

		},
		customReset: function() {
			$("#dropzone #upload-incentive").fadeIn(300); 
			myDropzone.files = [];
		}
	});
	

	var addActionsToFiles = function() {
		$('.row.files li ')
	}
	

	var deleteItem = function(filename, item) {

		$.ajax({
			url:'traitements-serveur.php?action=deleteOne',
			dataType: "json",
			data: { 'file':filename },
			success: function(data) {
				if (data.success) {
					$(item).fadeOut(500);
				} else {
					onError( data.message );
				}
			},
			error: function(error) {
				onError("Impossible de supprimer ce fichier.");
			}
		}).always(function() {
			$(item).removeAttr('disabled');
		});

	}


	var updateList = function() {

		// Affiche un loader
		$('.row.files').empty().html('<span class="loader"></span>');
		is_loading = true; 

		// Chargement de la liste des fichiers
		$.ajax({
			url:'traitements-serveur.php?action=listFiles',
			dataType: "json",
			success: function(data) {
				var tpl = $('script#FileItem').html();
				is_loading = false;
				if (!data.list) return onError("Impossible de charger la liste des fichiers");

				if (!data.list.length) {
					$('.row.files').empty().html("Aucun fichier .XLSX");
					return false;
				}

				listHtml = '<ul>';
				$.each(data.list, function(k,item) {
					listHtml+= tpl.replace("###FILENAME###", item);
				});
				listHtml += '</ul>';
				$('.row.files').empty().html(listHtml);

				$('[data-toggle="tooltip"]').tooltip();

				$('.row.files a').on('click', function(e) {
					
					if ($(this).attr('disabled')) return false;
					$(this).attr('disabled', 'disabled');

					var fileitem = $(this).parents('li.fileitem');
					if (!fileitem || !fileitem.length) return false;
					
					var name = $('.title-file-excel',fileitem).html();

					if ( $(this).hasClass('delete')){
						e.preventDefault();
						deleteItem( name, fileitem );
					} else if ($(this).hasClass('download')) {
						$(this).attr('href', 'traitements-serveur.php?action=downloadOne&file='+encodeURIComponent(name) );
						$(this).removeAttr('disabled');
					}
						
				});
				
			}, 
			error: function(error) {
				onError("Impossible de charger la liste des fichiers");
				is_loading = false; 
			}
		});
	}

	updateList(); // init

	$('.submenu .download a').on('click', function(e) {
		if ( !$('.files ul li.fileitem').length ) {
			e.preventDefault();
			onError("Il n'y a aucun fichier JSON a télécharger car vous n'avez importé aucun fichier .XLSX.", "Aucun fichier à télécharger");
		}
	});

	// debug
	window.debug = updateList;

});


 






