var BlockFactoryWrapper = function() {
	
	var templateFn = doT.template( $("script#BlockTemplate").html().replace(/&amp;/g, '&') );
	if (templateFn && typeof templateFn === 'function') $("script#BlockTemplate").empty().remove();

	this.getHtml = function(data, type) {
		data._type = type;
		return templateFn(data);
	}
}



var SlidingBlockQuestion = function() {
	var that = this;
	var templateFn = doT.template( $("script#SlidingBlockTemplate").html().replace(/&amp;/g, '&') );
	if (templateFn && typeof templateFn === 'function') $("script#SlidingBlockTemplate").empty().remove();

	var columnCount, swipperIndex, posX, posXmax, columnWidth, selectedColumn, isAnimated;
  		
	that.init = function(el) {
		
		// reinit
		$('.bloc .description', el).height('auto');
		$('')

		columnCount = $('.bloc', el).length;
  		isAnimated = false;
  		swipperIndex = 0;

  		var initPos = 30;
  		
		// Init Slidind Mask size and block width
		var blocMargin = parseInt( $('.bloc', el).first().css('margin-left'), 10) + parseInt($('.bloc', el).first().css('margin-right'), 10)
		var visibleArea = $('.sliding-mask', el).width();
		var blocListSize = 0;
		var maxBlockHeight = 0;

		// Reduit la du premier bloc
		//$('.bloc', el).first().css('margin-left', 5); // 5px au lieu de 15px

		var nbColumnVisible = 2;
		var incitativeMargin = 60 / nbColumnVisible; // on retranche qqs pixels de chaque bloc afin de laisser légérement apparaitre le premier bloc masqué et ainsi inciter au slide
		if ($(window).width() < 970) nbColumnVisible=1;
		$('.bloc', el).each( function(k,v) {
			//var is_first_or_last = (k===0 || k===columnCount-1 ? true : false);
			$(v).outerWidth(visibleArea/nbColumnVisible - blocMargin - incitativeMargin);
			blocListSize += $(v).outerWidth() + blocMargin;
		});
		
		$('.sliding-area', el).width( blocListSize );
		
		// --- Init moving 
		var updateArrowDisablingMode = function() {

			if (swipperIndex<columnCount-nbColumnVisible)
				$('a.arrow-right', el).removeClass('disabled');
			else 
				$('a.arrow-right', el).addClass('disabled');

			if (swipperIndex>0)
				$('a.arrow-left', el).removeClass('disabled');
			else 
				$('a.arrow-left', el).addClass('disabled');
		}


		var moveColumn = function(e) {
		    e.preventDefault();
		    if (isAnimated) return false;

		    if ( $(this).hasClass('arrow-right') && swipperIndex<columnCount-nbColumnVisible) {
		      posX -= columnWidth;
		      swipperIndex++;
		    }
		    if ( $(this).hasClass('arrow-left') && swipperIndex>0 ) {
		       posX += columnWidth; 
		       swipperIndex--;
		    }
		    updateArrowDisablingMode();
		    isAnimated = true;
		    $('.sliding-area', el).animate( { 'left' : posX}, 500, function() { isAnimated=false; } );
	    }

	    var showColumn = function(indexColumn) {
	      isAnimated = true;
	      console.log("l", indexColumn, swipperIndex);
	      swipperIndex = indexColumn;
	      posX = initPos +(-1*columnWidth*swipperIndex);
	      updateArrowDisablingMode();
	      $('.sliding-area', el).animate( { 'left' : posX }, 500, function() { isAnimated=false;  } );
	  	}

		$('.sliding-area', el).css('left', initPos );
		columnWidth = $('.bloc', el).first().outerWidth() + blocMargin;
  		posX = initPos;
    	posXmax = $('.sliding-area', el).width();
    	$('a.arrow-left, a.arrow-right', el).unbind().on('click', moveColumn);

		// Paragraph should have same height
		var firstBloc = $('.bloc', el).first();
		var countDescription = $('.description', firstBloc).length;
		var maxHeight;
		for (var i = 0; i < countDescription; i++) {
			maxHeight = 0;
		    $('.bloc', el).each( function(k,v) {
		    	maxHeight = Math.max(maxHeight, $($('.description',v).get(i)).height() );
		    });
		    $('.bloc', el).each( function(k,v) {
		    	$($('.description',v).get(i)).height(maxHeight);
		    });
		}

		// Fix mask height
		var maxBlockHeight;
		$('.bloc', el).each( function(k,v) {
			maxBlockHeight = Math.max( $(v).outerHeight() + 40, maxBlockHeight);
		});
		$('.sliding-mask', el).height( maxBlockHeight );

		// Arrow is following
		var arrowMiddleHeight = $('a.arrow-left, a.arrow-right', el).height()*0.5;
		$('.choices', el).unbind().on('mousemove', function(e) {
			e.preventDefault();
			var posY = $('a.arrow-left', el).position().top + arrowMiddleHeight;
			var y = e.pageY - e.currentTarget.offsetTop;
			if (y < 20 || y > $(this).height()-blocMargin*2 ) return false;
			$('a.arrow-left, a.arrow-right', el).css('top', y - arrowMiddleHeight );
		});

		$('.bloc', el).unbind().on('click', function(e) {
			
			e.preventDefault();
			var that = $(this);

			if (that.hasClass('disabled')) return false;

			var delay = 0;
			
			if (swipperIndex - that.index() === -2) { showColumn( swipperIndex+1 ); delay = 600; }
			else if (swipperIndex - that.index() === 1 && swipperIndex>=(columnCount-nbColumnVisible)) { showColumn( swipperIndex-1 ); delay = 600; }
			
			setTimeout( function() {
				app.selectedConvention.selectBloc( that, el );
			}, delay);

		});

		updateArrowDisablingMode();

	}

	that.getHtml = function(data) {
		console.log(data);
		return templateFn(data);
	}

}

var DefaultBlockListQuestion = function() {
	var that = this;
	
	that.getHtml = function(qdata) {
		var choices = "";
		$.each(qdata.choices, function(k,v) {
			choices += BlockFactory.getHtml(v, qdata.type )
		});	
		return choices;
	}

	that.init = function(el) {

		// Donne une hauteur constante à tous les blocs
		var maxHeight = 0;
		$('.bloc', el).each(function(k,v) {
			maxHeight = Math.max( maxHeight, $(v).height() );	
		});
		$('.bloc', el).height( maxHeight );

		// Centrer le titre dans le bloc si c'est un bloc sans description
		$('.bloc', el).each(function(k,v) {
			if ( $('.title.no-description',v).length ) {
				var emptySpace = $(v).height() - $('.bloc-container',v).outerHeight();
				$('.bloc-container',v).css({'margin-top': 0.5*emptySpace }); 
			}
		});

		// actions
		$('.bloc', el).unbind().on('click', function(e) {
			e.preventDefault();
			if ($(this).hasClass('disabled')) return false;
			app.selectedConvention.selectBloc( $(this), el );
		});
	}
}





var ResultStep = function() {
	var that = this;
	var templateFn = doT.template( $("script#ResultStep").html().replace(/&amp;/g, '&') );
	if (templateFn && typeof templateFn === 'function') $("script#ResultStep").empty().remove();
	var result = $('.result');

	that.display = function(data) {
		
		
		result.css('height','auto');
		result.empty().html( templateFn(data) ).removeClass('hidden');

		$('.emptyness').addClass('hidden');
		$('.result .done-picto').css('opacity', 0);
		
		// Gestion de la hauteur du div.results afin qu'il prenne 100% de la hauteur de la page
		var resultDefaultHeight = result.outerHeight();
		var fullResultHeight = $(window).height() - Scroll.defaultTopMargin;
		
		if (fullResultHeight > resultDefaultHeight) {
			result.css('height', fullResultHeight);
			Scroll.toDown();
		} else {
			Scroll.to( result );
		}

		$('.result .gototop').unbind().on('click', function(e) {
			e.preventDefault();
			Scroll.toTop();
		});
		
		setTimeout( function() {  
			$('.result .loading').addClass('hidden');
			$('.result .c .cc').removeClass('invisible');
			$('.result .done-picto').animate({"opacity":1}, 500);
		}, Math.random(1500)+800 ); // Durée de l'animations
	}

	that.hide = function() {
		result.addClass('hidden');
		$('.emptyness').removeClass('hidden');
	}
}
ResultStep = new ResultStep();



var BlockFactory = new BlockFactoryWrapper();


var dbl = new DefaultBlockListQuestion();
var sb = new SlidingBlockQuestion(); 
var QuestionTypes = {
	'block-title-description': dbl,
	'titled-block-with-icon': dbl,
	'sliding-block': sb
};



var Scroll = {
	
	'defaultTopMargin' : $('.header').outerHeight(),

	'to' : function(step) {
		$('html, body').animate({
			"scrollTop": $(step).offset().top - Scroll.defaultTopMargin
		}, 500, 'easeOutQuad' );
	},

	'toDown': function() {
		$('html, body').animate({
			"scrollTop": $(document).height() + Scroll.defaultTopMargin
		}, 500, 'easeOutQuad' );
	},

	'toTop': function() {
		$('html, body').animate({
			"scrollTop": -10
		}, 500, 'easeOutQuad' );
	}

}


var ConventionBuilder = function() {
	var that = this;
	
	var buildQuestion = function(qdata) {
		
		var splited = qdata.type.split("-");
		if (splited.length) {
			var last = splited[splited.length-1];
			if ( last.match(/^col[2,3,4]$/) ) {
				qdata.col = last;
				splited.pop();
				qdata.type = splited.join('-');
			}
		}
		
		var choices = QuestionTypes[qdata.type];
		if (!choices) alert("Le type de question "+qdata.type+" n'existe pas...");
			else choices = choices.getHtml( qdata );
		
		var q = {
			"id": qdata.id,
			"type": qdata.type,
			"col": qdata.col,
			"question": qdata._title,
			"label": (qdata._label ? qdata._label : " "),
			"choices": choices
		};

		var qhtml = app.createNewStep(q);
		$('.content').append( qhtml );
		var added = $('.content').children().last();
		QuestionTypes[qdata.type].init( added );
		return added;
	}


	that.build = function(_id, _data) { // First build (q1 cause it's launched from q0)

		
		var Convention = function(id, data) {
			this.id = id;
			this.data = data;
			this.meta = $.grep( app.conventions, function ( n ) { return n._index == _id;	})[0];
		};

		Convention.prototype.buildQuestion = buildQuestion;
		Convention.prototype.getStepById = function(stepId) {
			return $.grep( $('.content .step'), function ( n ) { 
			    return $(n).attr('data-id') == stepId;
			})[0];
		}
		Convention.prototype.nextQuestion = function(clickedBloc, nextStepId) {

			var choices = $( $(clickedBloc).parents('.choices') );

			Header.updateBreadcrumb();
			
			if (nextStepId==="RESULTS") {
				return app.selectedConvention.displayResult();
			} else {
				ResultStep.hide();
			}
			
			// Rajouter la question suivante
			var nextQ = $.grep( this.data.questions, function ( n ) { 
			    return n.id == nextStepId;
			});
			nextQ = (nextQ && nextQ.length ? nextQ[0] : false);
			if (!nextQ) {
				alert('La question id="'+nextStepId+'" n\'existe pas pour cette convention...');
				return false; //
			}

			var nextStepExists = this.getStepById(nextQ.id);
			
			if (!nextStepExists) {
				this.buildQuestion(nextQ);
				nextStepExists = this.getStepById(nextQ.id);
			}
			Scroll.to(nextStepExists);


			
		}; // end : Convention.prototype.nextQuestion


		Convention.prototype.selectBloc = function(clickedBloc, stepElement) {
			var nextStepId = $(clickedBloc).attr('data-next');
			var selectedNextStepId;
			var selected = $('.bloc.selected', stepElement);
			if (selected.length) { // S'il y a déjà une réponse selectionnée...
				selectedNextStepId = selected.attr('data-next');
				if (nextStepId !== selectedNextStepId) { // et que la prochaine question relative à la nouvelle réponse donné n'est pas la même que la précédente... 
					$('.content .step').each(function(k,v) {  // Alors tu recettes tout ce qui est après cette question
						if ( parseInt($(v).attr('data-index'),10) > parseInt($(clickedBloc).parents('.step').attr('data-index'),10) ) 
							$(v).empty().remove(); 
					});
				}
				selected.removeClass('selected');
			}
			
			clickedBloc.addClass('selected');
			this.nextQuestion( clickedBloc, nextStepId );
		} // end : Convention.prototype.selectBloc

		Convention.prototype.displayResult = function() {
			var points = 0;
			$('.content .step').each(function(k,s) {
				if (k===0) return true;
				var p = $('.bloc.selected', s).attr('data-points');
				if (isNaN(p)) {
					console.error("La réponse séléctionnée à la question index="+k+" n'a aucune podération.");
				} else {
					points += parseInt(p, 10);
				}
			});

			var the_result = false;
			
			$.each(this.data.results, function(p,R) {
				p = $.trim(p);
				if (p.indexOf('-')<0) {
					p = parseFloat(p);
					if (points==p) 
						the_result = R;
				}
				else {
					p = p.split('-');
					var pmin = parseFloat(p[0]);
					var pmax = parseFloat(p[1]);
					if (points>=pmin && points<=pmax) the_result = R;
				}
				if (the_result) 
					return false;	
			});

			if (the_result) {
				var rdata = { "label":the_result[0], "salary":the_result[1], "salary_freq":the_result[2] };
				ResultStep.display( rdata );
			} else {
				alert('Les configurations de la conventions n\'a pas prévu ce résultat.');
			}
		
		}
		
		var newConv = new Convention(_id, _data);

		if (_data.questions && _data.questions.length) {
			Header.change( newConv.meta._title, newConv.meta._icon);
			Header.updateBreadcrumb();
			Scroll.to( buildQuestion(_data.questions[0]) );
		}
			
		return newConv;
	}

	

}

var Header = function() {
	var that = this;
	var one = $('.header .layer.one');
	var two = $('.header .layer.two');
	var oneTop = -80;
	var twoTop = 0;
	var isAnimating = false;

	var breadcrumb = [];

	that.change = function(h1, picto) {
		if (isAnimating) 
			return setTimeout(function() { that.change(h1, picto); }, 610);
		isAnimating = true;

		$('h1',one).html(h1);
		$('.picto i',one).removeClass().addClass('fa fa-'+picto);

		one.animate({"top":twoTop}, 600, 'easeOutQuad');
		two.animate({"top":twoTop+100}, 600, 'easeOutQuad', function() {
			$('h1',two).html(h1);
			$('.picto i',two).removeClass().addClass('fa fa-'+picto);
			$(one).css({"top":oneTop});
			$(two).css({"top":twoTop});
			isAnimating = false;
		});	
	}

	that.updateBreadcrumb = function() {
		$('.header .fil-ariane').empty();
		var steps = $('.content .step');
		$('.header').removeClass('start');
		var next = "CONVENTIONS";
		steps.each(function(k,s) {
			var b = $('.bloc.selected', s);
			var i = b.attr('data-id');
			var n = $(s).attr('data-id');
			if (!i) return true;
			var tpl = '<a href="#'+n+'">'+i+'</a>';
			if (steps.length-1 !== k) tpl+="<span> > </span>";
			$('.header .fil-ariane').append( tpl );
		});

		$('.header .fil-ariane a').unbind().on('click', function(e) {
			e.preventDefault();
			var t = $(this).attr('href').substr(1);
			Scroll.to( app.selectedConvention.getStepById(t) );
		});
	}

}
Header = new Header();


var App = function(conventionsData) {

	var that = this;
	that.conventions = conventionsData;
	that.selectedConvention = null;
	
	var newStepTemplateFn = doT.template( $("script#NewStep").html().replace(/&amp;/g, '&') );
	if (newStepTemplateFn && typeof newStepTemplateFn === 'function') $("script#NewStep").empty().remove();
	
	that.createNewStep = function(data) {
		data.index = $('.content .step').length;
		$('.content').append( newStepTemplateFn(data) );
	}

	var conventionBuilder = new ConventionBuilder();

	// Begining
	$('.header').addClass('start');
	$('.header h1').html("KALI 3.0");
	$('.header .picto .i').removeClass().addClass('fa fa-file-text-o');

	// First Q
	var convChoices = "";
	$.each(that.conventions, function(k,v) {
		convChoices += BlockFactory.getHtml(v, "titled-block-with-icon")
	});


	var conventionChoice = {
		"id": "CONVENTIONS",
		"type": "titled-block-with-icon",
		"question": "De quelle convention collective dépendez-vous ?",
		"choices": convChoices
	};

	$('.content').html( that.createNewStep(conventionChoice) );
	$('.content .step .choices#Q0 .bloc').unbind().on('click', function(e) {
		e.preventDefault();
		if ($(this).hasClass('disabled')) return false;
		$('.content .step .choices#Q0 .bloc').removeClass('selected');
		$(this).addClass('selected');
		$('.content .step').each(function(k,v) { if (k>0) $(v).empty().remove(); });
		ResultStep.hide();

		var id = $(this).attr('data-id');
		$('.slide-loading').removeClass('hidden');
		$.getJSON( "data/"+id+".json", function( data ) {
			that.selectedConvention = conventionBuilder.build(id, data);
		}).fail(function(data) {
	    	alert('Impossible de charger la configuration de cette convention.');
	    	$('.content .step .choices#Q0 .bloc').removeClass('selected');
	    })
	    .always(function() {
	    	$('.slide-loading').addClass('hidden');
	    });
	});



}

var app;
$( document ).ready(function() {
	$.getJSON( "data/conventions.json", function( data ) {
		conventions = data;
		app = new App(data);
	}).fail(function(data) {
    	alert('Impossible de charger l\'index des conventions.');
    });

});
 






