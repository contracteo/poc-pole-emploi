# POC POLE-EMPLOI : la classification conventionnelle 

## L'application de démonstration 
Cette application web propose à l'utilisateur, à partir d'une convention collective choisie, de trouver la classification conventionnelle qui lui correspond.  
L'utilisateur est invité à choisir, via un simple clic, entre plusieurs éléments textuels issus directement des textes de la convention.
Si la réponse donnée par l'utilisateur ne suffit pas pour déterminer la classification conventionnelle, il se verra invité à répondre à une autre question.  
Ainsi, de clics en clics, l'utilisateur avance  dans l'arbre décisionnel (prévu par la convention collective). 
Quand l'application a suffisamment de données pour déduire la classification souhaitée, elle affiche la page de résultat. Cette page peut aussi indiquer le salaire minimum correspondant à la classification toruvée (si la convention collective précise cette information).

### Respect des contenus 
Les blocs de textes proposés à l'utilisateur sont **identiques** aux textes contenus dans la convention collective. 
C'est en jouant sur le décryptage de l'arbre décisionnel et une ergonomie simple et intuitive qu'on l'on peut proposer une façon didactique de résoudre la problématique de la classification conventionnelle.

### Accessible et facile à déployer
Afin qu'elle puisse être facilement partagée / publiée, nous avons choisi de développer cette démonstration sous le format d'une application-web.  
La technologie utilisée se limite à : HTML5 / CSS / JavaScript.
Un effort a été fait pour que l'application puisse s'adapter à de très nombreux formats d'écrans (écrans classiques, écran 16/9, tablettes...) puisque la technologie utilisée le permet.

### Évolutive
L'application prévoit un moyen simple et complet pour permettre l'édition et le rajout de contenus. 
Ainsi, le nombre de conventions collectives intégrées n'est pas limité.  
L'application est alimenté par des fichiers de configuration (format JSON) qui décrivent l'arbre décisionnel et les contenus des choix-multiples.
Ces fichiers JSON peuvent être très simplement générer via notre `outil-convertisseur-xlsx` à partir de fichiers .xlsx (Office Open XML) issus de logiciels tels que Microsoft-Excel, Open-Office Tableur, ou encore Google-Spreadsheet.

### Installation 
Le dossier "POC" (pour Proof-Of-Concept) contient tous les fichiers nécéssaires au bon fonctionnement de l'application. Aucune dépendance n'est a prévoir.  
Il suffit d'accéder au fichier 'index.html` par le protocole HTTP, via un navigateur web moderne pour lancer l'application.

Les fichiers *.json* qui alimentent le contenu de l'application doivent être déposés dans le dossier "/data".  

----
## Le convertisseur de fichier .xlsx en .json
L'application de démonstration est alimentée par des fichiers au format *.json* .
Ce format de fichier est libre et a l'avantage d'être nativement interprété par la plupart des navigateurs web. 
Cependant, il peut être très contraignant de rédiger de bout en bout, de mettre à jour, et de maintenir un fichier dans ce format (un seul caractère manquant ou mal placé le rend invalide et illisible).
Pour répondre à cette problématique "technique", nous avons mis en place une norme simple sous forme de tableurs (fichiers .xlsx) pour décrire les données que ces fichiers **.json** doivent contenir.  

Les logiciels tels que Microsoft-Excel, Open-Office Tableur, ou encore Google-Spreadsheet, savent produire des fichiers .xlsx (Office Open XML).  
Ainsi, ces logiciels de bureautique (connus de tous) suffisent pour créer ou mettre à jour le contenu de l'application.
En effet, grâce à l'outil de conversion que nous avons développé, chaque fichier *.xlsx* qui respecte notre norme peut être converti en un fichier *.json* lisible par l'application.

Ce convertisseur se présente aussi sous la forme d'une application-web (HTML5 / CSS / JS) mais nécéssite des traitements côté serveur (scripts en PHP).

### Installation 
Le dossier "outil-convertisseur-xlsx" contient tous les fichiers nécessaires au bon fonctionnement de l'application.  
Un serveur avec PHP 5.4 (ou supérieur) est requis pour l'exécution.
De plus, la bibliothèque `phpexcel` est nécessaire pour le bon fonctionnement de l'outil. Cette dépendance s'installe simplement via `composer` par un simple : 
```
composer install
```
Une fois ces pré-requis mis en place, il suffit d'accéder au fichier `index.html` via un navigateur web moderne pour lancer l'application.

Les fichiers *.xlsx* sont déposés dans le dossier "/imports" et les fichiers créés dans le dossier "/exports".  
**Nb :** Le dossier "/exports" peut-être remplacé par un lien symbolique pointant vers le dossier "/data" de l'application de démonstration. Ainsi, les fichiers .json générés par le convertisseur se retrouvent directement actifs dans l'application sans que vous n'ayez à les déplacer.
